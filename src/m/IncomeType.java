/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Me
 */
public class IncomeType {

    private Session session=null;
    
    public pojo.IncomeType getBy(int id) {
        session=conn.NewHibernateUtil.getSessionFactory().openSession();
        try {
            pojo.IncomeType IncomeType = (pojo.IncomeType) session.createCriteria(pojo.IncomeType.class).add(Restrictions.eq("id", id)).uniqueResult();
            return IncomeType;
        } catch (Exception e) {
            return null;
        }finally{
            session.close();
        }
    }

    public List<pojo.MonyIncome> getAll() {
        session=conn.NewHibernateUtil.getSessionFactory().openSession();
        try {
            List<pojo.MonyIncome>  list = session.createCriteria(pojo.IncomeType.class).list();
            return list;
        } catch (Exception e) {
            return null;
        }finally{
            session.close();
        }
    }
}
