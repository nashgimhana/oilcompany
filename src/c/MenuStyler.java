/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 *
 * @author suhada
 */
public class MenuStyler implements MouseListener{

    public static MenuStyler MenuStyler=null;
    public static MenuStyler getInstance(){
        if(MenuStyler==null)MenuStyler=new MenuStyler();return MenuStyler;
    }
    
    @Override
    public void mouseClicked(MouseEvent e) {
        Component comp = e.getComponent();
        Container parent = comp.getParent();
        Component[] components = parent.getComponents();
        if(components.length>0){
            int clen=components.length;
            for (int i = 0; i < clen; i++) {
                components[i].setBackground(Color.WHITE);
            }
            comp.setBackground(c.View.getInstance().colorMenuSelet);
        }
        if(comp.getAccessibleContext().getAccessibleName().equalsIgnoreCase("status")){
            c.View.getInstance().setStatusViewToDPane(v.Home.getInstance().dpnl_contain);
        }else if(comp.getAccessibleContext().getAccessibleName().equalsIgnoreCase("stock")){
            c.View.getInstance().setStockViewToDPane(v.Home.getInstance().dpnl_contain);
        }else if(comp.getAccessibleContext().getAccessibleName().equalsIgnoreCase("employee")){
            c.View.getInstance().setEmployeeViewToDPane(v.Home.getInstance().dpnl_contain);
        }else if(comp.getAccessibleContext().getAccessibleName().equalsIgnoreCase("Cash/Check Book")){
            c.View.getInstance().setCashCheckBookViewToDPane(v.Home.getInstance().dpnl_contain);
        }else if(comp.getAccessibleContext().getAccessibleName().equalsIgnoreCase("Vehicle")){
            c.View.getInstance().setVehicleViewToDPane(v.Home.getInstance().dpnl_contain);
        }else if(comp.getAccessibleContext().getAccessibleName().equalsIgnoreCase("Supplier")){
            c.View.getInstance().setSupplierView(v.Home.getInstance().dpnl_contain);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        Component component = e.getComponent();
        if(component.getBackground()!=c.View.getInstance().colorMenuSelet)
        component.setBackground(c.View.getInstance().colorMenuOver);
    }

    @Override
    public void mouseExited(MouseEvent e) {
        Component component = e.getComponent();
        if(component.getBackground()!=c.View.getInstance().colorMenuSelet)
        component.setBackground(Color.WHITE);
    }
    
}
