package pojo;
// Generated Jun 24, 2017 11:26:27 PM by Hibernate Tools 4.3.1



/**
 * InvoiceLog generated by hbm2java
 */
public class InvoiceLog  implements java.io.Serializable {


     private Integer id;
     private GrnLog grnLog;
     private Invoice invoice;
     private Double qty;
     private Double price;

    public InvoiceLog() {
    }

	
    public InvoiceLog(GrnLog grnLog, Invoice invoice) {
        this.grnLog = grnLog;
        this.invoice = invoice;
    }
    public InvoiceLog(GrnLog grnLog, Invoice invoice, Double qty, Double price) {
       this.grnLog = grnLog;
       this.invoice = invoice;
       this.qty = qty;
       this.price = price;
    }
   
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    public GrnLog getGrnLog() {
        return this.grnLog;
    }
    
    public void setGrnLog(GrnLog grnLog) {
        this.grnLog = grnLog;
    }
    public Invoice getInvoice() {
        return this.invoice;
    }
    
    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }
    public Double getQty() {
        return this.qty;
    }
    
    public void setQty(Double qty) {
        this.qty = qty;
    }
    public Double getPrice() {
        return this.price;
    }
    
    public void setPrice(Double price) {
        this.price = price;
    }




}


