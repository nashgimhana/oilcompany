/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m;

import java.util.regex.Pattern;

/**
 *
 * @author suhada
 */
public class ValueValidation {
    public static ValueValidation ValueValidation=null;
    public static ValueValidation getInstance(){
        if(ValueValidation==null)ValueValidation=new ValueValidation();
        return ValueValidation;
    }
    
    /**
     *  This method is for check nic
     * @param value The Value that need to check
     * @return  will return a boolean value if values is valid will return 'true'. else 'false';
     */
    public boolean isNIC(String value){
        if(!value.isEmpty())
        return Pattern.matches("^[0-9]{9}[VvXx]{1}$", value);
        else return false;
    }
    
    /**
     *  This method is for check number
     * @param value The Value that need to check
     * @return  will return a boolean value if values is valid will return 'true'. else 'false';
     */
    public void isNumber(String value){
        
    }
    
    /**
     *  This method is for check text
     * @param value The Value that need to check
     * @return  will return a boolean value if values is valid will return 'true'. else 'false';
     */
    public boolean isText(String value){
        if(!value.isEmpty())
        return Pattern.matches("^\\w+[A-Za-z]$", value);
        else return false;
    }
    
    /**
     *  This method is for check email
     * @param value The Value that need to check
     * @return  will return a boolean value if values is valid will return 'true'. else 'false';
     */
    public boolean isEmail(String value){
        if(!value.isEmpty())
        return Pattern.matches("^\\w+@\\w+.\\w+$", value);
        else return false;
    }
    
    /**
     *  This method is for check contact number sri lankan form
     * @param value The Value that need to check
     * @return  will return a boolean value if values is valid will return 'true'. else 'false';
     */
    public boolean isContactNumber(String value){
        if(!value.isEmpty())
        return Pattern.matches("^\\d{10}$", value);
        else return false;
    }
}
