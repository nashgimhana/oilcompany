package pojo;
// Generated Jun 24, 2017 11:26:27 PM by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;

/**
 * GrnLog generated by hbm2java
 */
public class GrnLog  implements java.io.Serializable {


     private Integer id;
     private Grn grn;
     private Product product;
     private Double buyingPrice;
     private Double quantity;
     private Double remainingQuantity;
     private Set invoiceLogs = new HashSet(0);
     private Set productionLogMixes = new HashSet(0);
     private Set productionLogBottles = new HashSet(0);
     private Set deliveryItemLogs = new HashSet(0);
     private Set shopTransferLogs = new HashSet(0);

    public GrnLog() {
    }

	
    public GrnLog(Grn grn, Product product) {
        this.grn = grn;
        this.product = product;
    }
    public GrnLog(Grn grn, Product product, Double buyingPrice, Double quantity, Double remainingQuantity, Set invoiceLogs, Set productionLogMixes, Set productionLogBottles, Set deliveryItemLogs, Set shopTransferLogs) {
       this.grn = grn;
       this.product = product;
       this.buyingPrice = buyingPrice;
       this.quantity = quantity;
       this.remainingQuantity = remainingQuantity;
       this.invoiceLogs = invoiceLogs;
       this.productionLogMixes = productionLogMixes;
       this.productionLogBottles = productionLogBottles;
       this.deliveryItemLogs = deliveryItemLogs;
       this.shopTransferLogs = shopTransferLogs;
    }
   
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    public Grn getGrn() {
        return this.grn;
    }
    
    public void setGrn(Grn grn) {
        this.grn = grn;
    }
    public Product getProduct() {
        return this.product;
    }
    
    public void setProduct(Product product) {
        this.product = product;
    }
    public Double getBuyingPrice() {
        return this.buyingPrice;
    }
    
    public void setBuyingPrice(Double buyingPrice) {
        this.buyingPrice = buyingPrice;
    }
    public Double getQuantity() {
        return this.quantity;
    }
    
    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }
    public Double getRemainingQuantity() {
        return this.remainingQuantity;
    }
    
    public void setRemainingQuantity(Double remainingQuantity) {
        this.remainingQuantity = remainingQuantity;
    }
    public Set getInvoiceLogs() {
        return this.invoiceLogs;
    }
    
    public void setInvoiceLogs(Set invoiceLogs) {
        this.invoiceLogs = invoiceLogs;
    }
    public Set getProductionLogMixes() {
        return this.productionLogMixes;
    }
    
    public void setProductionLogMixes(Set productionLogMixes) {
        this.productionLogMixes = productionLogMixes;
    }
    public Set getProductionLogBottles() {
        return this.productionLogBottles;
    }
    
    public void setProductionLogBottles(Set productionLogBottles) {
        this.productionLogBottles = productionLogBottles;
    }
    public Set getDeliveryItemLogs() {
        return this.deliveryItemLogs;
    }
    
    public void setDeliveryItemLogs(Set deliveryItemLogs) {
        this.deliveryItemLogs = deliveryItemLogs;
    }
    public Set getShopTransferLogs() {
        return this.shopTransferLogs;
    }
    
    public void setShopTransferLogs(Set shopTransferLogs) {
        this.shopTransferLogs = shopTransferLogs;
    }




}


