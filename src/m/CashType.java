/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Me
 */
public class CashType {
    
    private Session session=null;

    public pojo.CashType getBY(int id) {
        session=conn.NewHibernateUtil.getSessionFactory().openSession();
        try {
            pojo.CashType CashType = (pojo.CashType) session.createCriteria(pojo.CashType.class).add(Restrictions.eq("id", id)).uniqueResult();
            return CashType;
        } catch (Exception e) {
            return null;
        }finally{
            session.close();
        }
    }

    public pojo.CashType getBy(String name) {
        session=conn.NewHibernateUtil.getSessionFactory().openSession();
        try {
            pojo.CashType CashType = (pojo.CashType) session.createCriteria(pojo.CashType.class).add(Restrictions.eq("name", name)).uniqueResult();
            return CashType;
        } catch (Exception e) {
            return null;
        }finally{
            session.close();
        }
    }

    public List<pojo.CashType> getAll() {
        session=conn.NewHibernateUtil.getSessionFactory().openSession();
        try {
            List<pojo.CashType> list = session.createCriteria(pojo.CashType.class).list();
            return list;
        } catch (Exception e) {
            return null;
        }finally{
            session.close();
        }
    }
}
