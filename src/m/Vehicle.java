/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Me
 */
public class Vehicle {

    private SessionFactory sessionFactory;

    public Vehicle() {
        sessionFactory = conn.NewHibernateUtil.getSessionFactory();
    }

    public pojo.Vehicle getById(int id) {
        return null;
    }

    public List<pojo.Vehicle> getByNumOne(String num1) {
        return null;
    }

    public List<pojo.Vehicle> getByNumTwo(String num2) {
        return null;
    }

    /**
     *
     * @param vehicle
     * @return return a String object, values as "not", "done", "error"
     */
    public String save(pojo.Vehicle vehicle) {
        Session openSession = sessionFactory.openSession();
        try {
            Transaction beginTransaction = openSession.beginTransaction();
            openSession.saveOrUpdate(vehicle);
            beginTransaction.commit();
            openSession.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            openSession.close();
        }
        return "not";
    }

    /**
     *
     * @param vehicle
     * @return return a String object, values as "not", "done", "error"
     */
    public String updateVehicle(pojo.Vehicle vehicle) {
        return "not";
    }

    public ArrayList<pojo.Vehicle> viewAllVehicle() {
        Session openSession = sessionFactory.openSession();
        ArrayList<pojo.Vehicle> vs = null;
        try {
            Criteria createCriteria = openSession.createCriteria(pojo.Vehicle.class);
            vs = (ArrayList<pojo.Vehicle>) createCriteria.list();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            openSession.close();
        }
        return vs;
    }

    public ArrayList<pojo.Vehicle> regNumber1() {
        Session openSession = sessionFactory.openSession();
        ArrayList<pojo.Vehicle> vs = null;
        try {
            Criteria createCriteria = openSession.createCriteria(pojo.Vehicle.class);
            vs = (ArrayList<pojo.Vehicle>) createCriteria.list();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            openSession.close();
        }
        return vs;
    }
}
