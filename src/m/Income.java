/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Me
 */
public class Income {

    private Session session=null;
    
    public List<pojo.MonyIncome> getAll(){
        session=conn.NewHibernateUtil.getSessionFactory().openSession();
        try {
            List<pojo.MonyIncome> list = session.createCriteria(pojo.MonyIncome.class).list();
            return list;
        } catch (Exception e) {
            return null;
        }finally{
            session.close();
        }
    }
    public List<pojo.MonyIncome> getAllBy(pojo.CashType type){
        session=conn.NewHibernateUtil.getSessionFactory().openSession();
        try {
            List<pojo.MonyIncome> list = session.createCriteria(pojo.MonyIncome.class).add(Restrictions.eq("cashType", type)).list();
            return list;
        } catch (Exception e) {
            return null;
        }finally{
            session.close();
        }
    }
    public List<pojo.MonyIncome> getAllBy(pojo.IncomeType type){
        session=conn.NewHibernateUtil.getSessionFactory().openSession();
        try {
            List<pojo.MonyIncome> list = session.createCriteria(pojo.MonyIncome.class).add(Restrictions.eq("incomeType", type)).list();
            return list;
        } catch (Exception e) {
            return null;
        }finally{
            session.close();
        }
    }
    public pojo.MonyIncome getBy(int id) {
        session=conn.NewHibernateUtil.getSessionFactory().openSession();
        try {
            pojo.MonyIncome MonyIncome = (pojo.MonyIncome) session.createCriteria(pojo.CashType.class).add(Restrictions.eq("id", id)).uniqueResult();
            return MonyIncome;
        } catch (Exception e) {
            return null;
        }finally{
            session.close();
        }
    }
    public List<pojo.MonyIncome> getAllBy(Date date) {
        session=conn.NewHibernateUtil.getSessionFactory().openSession();
        try {
            List<pojo.MonyIncome> list = session.createCriteria(pojo.MonyIncome.class).add(Restrictions.eq("date", date)).list();
            return list;
        } catch (Exception e) {
            return null;
        }finally{
            session.close();
        }
    }
    public List<pojo.MonyIncome> getAllBy(Date date,pojo.CashType ctype) {
        session=conn.NewHibernateUtil.getSessionFactory().openSession();
        try {
            List<pojo.MonyIncome> list = session.createCriteria(pojo.MonyIncome.class).add(Restrictions.and(Restrictions.eq("date", date),Restrictions.eq("cashType", ctype))).list();
            return list;
        } catch (Exception e) {
            return null;
        }finally{
            session.close();
        }
    }
    public List<pojo.MonyIncome> getAllBy(Date date,pojo.IncomeType type) {
        session=conn.NewHibernateUtil.getSessionFactory().openSession();
        try {
            List<pojo.MonyIncome> list = session.createCriteria(pojo.MonyIncome.class).add(Restrictions.and(Restrictions.eq("date", date),Restrictions.eq("incomeType", type))).list();
            return list;
        } catch (Exception e) {
            return null;
        }finally{
            session.close();
        }
    }
    public List<pojo.MonyIncome> getAllBy(Date date,pojo.IncomeType type,pojo.CashType ctype) {
        session=conn.NewHibernateUtil.getSessionFactory().openSession();
        try {
            List<pojo.MonyIncome> list = session.createCriteria(pojo.MonyIncome.class).add(Restrictions.and(Restrictions.eq("date", date),Restrictions.eq("incomeType", type),Restrictions.eq("cashType", ctype))).list();
            return list;
        } catch (Exception e) {
            return null;
        }finally{
            session.close();
        }
    }
    public List<pojo.MonyIncome> getAllBy(Date from, Date to){
        session=conn.NewHibernateUtil.getSessionFactory().openSession();
        try {
            List<pojo.MonyIncome> list = session.createCriteria(pojo.MonyIncome.class).add(Restrictions.between("date", from, to)).list();
            return list;
        } catch (Exception e) {
            return null;
        }finally{
            session.close();
        }
    }
    public List<pojo.MonyIncome> getAllBy(Date from, Date to,pojo.CashType ctype){
        session=conn.NewHibernateUtil.getSessionFactory().openSession();
        try {
            List<pojo.MonyIncome> list = session.createCriteria(pojo.MonyIncome.class).add(Restrictions.and(Restrictions.between("date", from, to),Restrictions.eq("cashType", ctype))).list();
            return list;
        } catch (Exception e) {
            return null;
        }finally{
            session.close();
        }
    }
    public List<pojo.MonyIncome> getAllBy(Date from, Date to,pojo.IncomeType type){
        session=conn.NewHibernateUtil.getSessionFactory().openSession();
        try {
            List<pojo.MonyIncome> list = session.createCriteria(pojo.MonyIncome.class).add(Restrictions.and(Restrictions.between("date", from, to),Restrictions.eq("incomeType", type))).list();
            return list;
        } catch (Exception e) {
            return null;
        }finally{
            session.close();
        }
    }
    public List<pojo.MonyIncome> getAllBy(Date from, Date to,pojo.IncomeType type,pojo.CashType ctype){
        session=conn.NewHibernateUtil.getSessionFactory().openSession();
        try {
            List<pojo.MonyIncome> list = session.createCriteria(pojo.MonyIncome.class).add(Restrictions.and(Restrictions.between("date", from, to),Restrictions.eq("incomeType", type),Restrictions.eq("cashType", ctype))).list();
            return list;
        } catch (Exception e) {
            return null;
        }finally{
            session.close();
        }
    }
        /**
     *
     * @param monyIncome
     * @return return a String object, values as "not", "done", "error"
     */
    public String save(pojo.MonyIncome monyIncome) {
        session=conn.NewHibernateUtil.getSessionFactory().openSession();
        try {
            Transaction bt = session.beginTransaction();
            Serializable save = session.save(monyIncome);
            bt.commit();
            session.flush();
            if(Integer.parseInt(save.toString())>0){
                return "done";
            }else return "not";
        } catch (Exception e) {
            return "error";
        }finally{
            session.close();
        }
    }
    /**
     *
     * @param monyIncome
     * @return return a String object, values as "not", "done", "error"
     */
    public String update(pojo.MonyIncome monyIncome) {
        session=conn.NewHibernateUtil.getSessionFactory().openSession();
        try {
            Transaction bt = session.beginTransaction();
            session.update(monyIncome);
            bt.commit();
            session.flush();
            return "done";
        } catch (Exception e) {
            return "error";
        }finally{
            session.close();
        }
    }
}
