/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c;

import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author suhada
 */
public class GetIncomeCashDetails {
    /**
     * Get Income cash details among a period
     * @param from start date
     * @param to  end date
     */
    public void get(Date from, Date to){
        try {
            if(from!=null){
            if(to!=null){
                
            }else JOptionPane.showMessageDialog(null, "Please enter start end", "Warning", JOptionPane.WARNING_MESSAGE);
            }else JOptionPane.showMessageDialog(null, "Please enter start date", "Warning", JOptionPane.WARNING_MESSAGE);
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Something went wrong.", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    /**
     * Get Income cash details among a period with income type
     * @param from start date
     * @param to end date
     * @param type income type
     */
    public void get(Date from, Date to,pojo.IncomeType type){
        try {
            if(from!=null){
            if(to!=null){
            if(type!=null){
                
            }else JOptionPane.showMessageDialog(null, "Please enter type", "Warning", JOptionPane.WARNING_MESSAGE);
            }else JOptionPane.showMessageDialog(null, "Please enter start end", "Warning", JOptionPane.WARNING_MESSAGE);
            }else JOptionPane.showMessageDialog(null, "Please enter start date", "Warning", JOptionPane.WARNING_MESSAGE);
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Something went wrong.", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    /**
     *  Get Income cash details on a date
     * @param day date
     */
    public void get(Date day){
        try {
            if(day!=null){
               
            }else JOptionPane.showMessageDialog(null, "Please enter date", "Warning", JOptionPane.WARNING_MESSAGE);
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Something went wrong.", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    /**
     * Get Income cash details on a date 
     * @param day date
     * @param type  income type
     */
    public void get(Date day,pojo.IncomeType type){
        try {
            if(day!=null){
            if(type!=null){
               
            }else JOptionPane.showMessageDialog(null, "Please enter type", "Warning", JOptionPane.WARNING_MESSAGE);
            }else JOptionPane.showMessageDialog(null, "Please enter date", "Warning", JOptionPane.WARNING_MESSAGE);
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Something went wrong.", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
}
