/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Me
 */
public class ExpenditureType {

    private Session session = null;

    public pojo.ExpenditureType getBy(int id) {
        session = conn.NewHibernateUtil.getSessionFactory().openSession();
        try {
            pojo.ExpenditureType ExpenditureType = (pojo.ExpenditureType) session.createCriteria(pojo.ExpenditureType.class).add(Restrictions.eq("id", id)).uniqueResult();
            return ExpenditureType;
        } catch (Exception e) {
            return null;
        } finally {
            session.close();
        }
    }
    public List<pojo.ExpenditureType> getAll() {
        session=conn.NewHibernateUtil.getSessionFactory().openSession();
        try {
            List<pojo.ExpenditureType>  list = session.createCriteria(pojo.ExpenditureType.class).list();
            return list;
        } catch (Exception e) {
            return null;
        }finally{
            session.close();
        }
    }
}
