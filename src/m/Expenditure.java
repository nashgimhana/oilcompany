/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m;

import java.util.Date;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Me
 */
public class Expenditure {

    private Session session = null;

    public pojo.MoneyExpenditure getBy(int id) {
        session = conn.NewHibernateUtil.getSessionFactory().openSession();
        try {
            pojo.MoneyExpenditure MoneyExpenditure = (pojo.MoneyExpenditure) session.createCriteria(pojo.MoneyExpenditure.class).add(Restrictions.eq("id", id)).uniqueResult();
            return MoneyExpenditure;
        } catch (Exception e) {
            return null;
        } finally {
            session.close();
        }
    }
    public List<pojo.MoneyExpenditure> getAll() {
        session=conn.NewHibernateUtil.getSessionFactory().openSession();
        try {
            List<pojo.MoneyExpenditure> list = session.createCriteria(pojo.MoneyExpenditure.class).list();
            return list;
        } catch (Exception e) {
            return null;
        }finally{
            session.close();
        }
    }
    public List<pojo.MoneyExpenditure> getAllBy(Date date) {
        session=conn.NewHibernateUtil.getSessionFactory().openSession();
        try {
            List<pojo.MoneyExpenditure> list = session.createCriteria(pojo.MoneyExpenditure.class).add(Restrictions.eq("date", date)).list();
            return list;
        } catch (Exception e) {
            return null;
        }finally{
            session.close();
        }
    }
    public List<pojo.MoneyExpenditure> getAllBy(Date date,pojo.CashType type) {
        session=conn.NewHibernateUtil.getSessionFactory().openSession();
        try {
            List<pojo.MoneyExpenditure> list = session.createCriteria(pojo.MoneyExpenditure.class).add(Restrictions.eq("date", date)).add(Restrictions.eq("cashType", type)).list();
            return list;
        } catch (Exception e) {
            return null;
        }finally{
            session.close();
        }
    }
    public List<pojo.MoneyExpenditure> getAllBy(Date date,pojo.ExpenditureType type) {
        session=conn.NewHibernateUtil.getSessionFactory().openSession();
        try {
            List<pojo.MoneyExpenditure> list = session.createCriteria(pojo.MoneyExpenditure.class).add(Restrictions.eq("date", date)).add(Restrictions.eq("expenditureType", type)).list();
            return list;
        } catch (Exception e) {
            return null;
        }finally{
            session.close();
        }
    }
    public List<pojo.MoneyExpenditure> getAllBy(Date date,pojo.ExpenditureType etype,pojo.CashType ctype) {
        session=conn.NewHibernateUtil.getSessionFactory().openSession();
        try {
            List<pojo.MoneyExpenditure> list = session.createCriteria(pojo.MoneyExpenditure.class).add(Restrictions.eq("date", date)).add(Restrictions.eq("expenditureType", etype)).add(Restrictions.eq("cashType", ctype)).list();
            return list;
        } catch (Exception e) {
            return null;
        }finally{
            session.close();
        }
    }
    public List<pojo.MoneyExpenditure> getAllBy(pojo.CashType type) {
        session=conn.NewHibernateUtil.getSessionFactory().openSession();
        try {
            List<pojo.MoneyExpenditure> list = session.createCriteria(pojo.MoneyExpenditure.class).add(Restrictions.eq("cashType", type)).list();
            return list;
        } catch (Exception e) {
            return null;
        }finally{
            session.close();
        }
    }
    public List<pojo.MoneyExpenditure> getAllBy(pojo.ExpenditureType type) {
        session=conn.NewHibernateUtil.getSessionFactory().openSession();
        try {
            List<pojo.MoneyExpenditure> list = session.createCriteria(pojo.MoneyExpenditure.class).add(Restrictions.eq("expenditureType", type)).list();
            return list;
        } catch (Exception e) {
            return null;
        }finally{
            session.close();
        }
    }
    public List<pojo.MoneyExpenditure> getAllBy(Date from,Date to) {
        session=conn.NewHibernateUtil.getSessionFactory().openSession();
        try {
            List<pojo.MoneyExpenditure> list = session.createCriteria(pojo.MoneyExpenditure.class).add(Restrictions.between("date", from, to)).list();
            return list;
        } catch (Exception e) {
            return null;
        }finally{
            session.close();
        }
    }
    public List<pojo.MoneyExpenditure> getAllBy(Date from,Date to,pojo.CashType type) {
        session=conn.NewHibernateUtil.getSessionFactory().openSession();
        try {
            List<pojo.MoneyExpenditure> list = session.createCriteria(pojo.MoneyExpenditure.class).add(Restrictions.and(Restrictions.between("date", from, to),Restrictions.eq("cashType", type))).list();
            return list;
        } catch (Exception e) {
            return null;
        }finally{
            session.close();
        }
    }
    public List<pojo.MoneyExpenditure> getAllBy(Date from,Date to,pojo.ExpenditureType type) {
        session=conn.NewHibernateUtil.getSessionFactory().openSession();
        try {
            List<pojo.MoneyExpenditure> list = session.createCriteria(pojo.MoneyExpenditure.class).add(Restrictions.and(Restrictions.between("date", from, to),Restrictions.eq("expenditureType", type))).list();
            return list;
        } catch (Exception e) {
            return null;
        }finally{
            session.close();
        }
    }
    public List<pojo.MoneyExpenditure> getAllBy(Date from,Date to,pojo.CashType ctype,pojo.ExpenditureType etype) {
        session=conn.NewHibernateUtil.getSessionFactory().openSession();
        try {
            List<pojo.MoneyExpenditure> list = session.createCriteria(pojo.MoneyExpenditure.class).add(Restrictions.and(Restrictions.between("date", from, to),Restrictions.eq("cashType", ctype),Restrictions.eq("expenditureType", etype))).list();
            return list;
        } catch (Exception e) {
            return null;
        }finally{
            session.close();
        }
    }
    public Long getTotal() {
        return null;
    }

}
