/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import v.Status;
import javax.swing.JDesktopPane;
import v.Cash_Check_Book;
import v.Customer;
import v.CustomerView;
import v.Employee;
import v.Stock;
import v.SuppllierView;
import v.Vehicle;

/**
 *
 * @author suhada
 */
public class View {

    public Color colorTheme = new Color(82, 188, 77, 255);
    public Color colorMenuSelet = new Color(82, 188, 77, 255);
    public Color colorMenuOver = new Color(153, 244, 113, 255);
    public static View View = null;

    public static View getInstance() {
        if (View == null) {
            View = new View();
        }
        return View;
    }

    public void setViewToDPane(JDesktopPane comp, Component c) {
        comp.removeAll();
        c.setSize(comp.getWidth(), comp.getHeight());
        comp.add("Status", c);
        c.setVisible(true);
    }

    public void setStatusViewToDPane(JDesktopPane comp) {
        comp.removeAll();
        Status instance = v.Status.getInstance();
        instance.setSize(comp.getWidth(), comp.getHeight());
        comp.add("Status", instance);
        instance.setVisible(true);
    }

    public void setCashCheckBookViewToDPane(JDesktopPane comp) {
        comp.removeAll();
        Cash_Check_Book instance = v.Cash_Check_Book.getInstance();
        instance.setSize(comp.getWidth(), comp.getHeight());
        comp.add("Status", instance);
        instance.setVisible(true);
    }

    public void setEmployeeViewToDPane(JDesktopPane comp) {
        comp.removeAll();
        Employee instance = v.Employee.getInstance();
        instance.setSize(comp.getWidth(), comp.getHeight());
        comp.add("Status", instance);
        instance.setVisible(true);
    }

    public void setStockViewToDPane(JDesktopPane comp) {
        comp.removeAll();
        Stock instance = v.Stock.getInstance();
        instance.setSize(comp.getWidth(), comp.getHeight());
        comp.add("Status", instance);
        instance.setVisible(true);
    }

    public void setVehicleViewToDPane(JDesktopPane comp) {
        comp.removeAll();
        Vehicle instance = v.Vehicle.getInstance();
        instance.setSize(comp.getWidth(), comp.getHeight());
        comp.add("Status", instance);
        instance.setVisible(true);
    }

    public void setSupplierView(JDesktopPane comp) {
        comp.removeAll();
        SuppllierView instance = v.SuppllierView.getInstance();
        instance.setSize(comp.getWidth(), comp.getHeight());
        comp.add("Status", instance);
        instance.setVisible(true);
    }

    public void setCutomerViewToDPane(JDesktopPane comp) {
        comp.removeAll();
        CustomerView instance = v.CustomerView.getInstance();
        instance.setSize(comp.getWidth(), comp.getHeight());
        comp.add("Status", instance);
        instance.setVisible(true);
    }
}
